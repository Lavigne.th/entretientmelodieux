﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DelayReaction : MonoBehaviour {

    public float delay;

	// Use this for initialization
	void Start () {
        Invoke("react", delay);
	}

    private void react()
    {
        GetComponent<Animator>().SetTrigger("react");
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
