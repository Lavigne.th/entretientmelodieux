﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StartBtnController : MonoBehaviour {

    public int sceneIndex;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void nextScene()
    {
        GameController.controller.resetScore();
        SceneManager.LoadScene(sceneIndex);
    }
}
