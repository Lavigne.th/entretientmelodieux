﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Assets.Scripts.Interview;

public class GameController : MonoBehaviour {

    public static GameController controller;
    public int score, maximumScore = 0;

    public InterviewData interviewData;

    private Slider scoreSlider;

    void Awake()
    {
        if (controller == null)
        {
            DontDestroyOnLoad(gameObject);
            controller = this;
            interviewData = new InterviewData();
        }
        else if (controller != this)
        {
            Destroy(gameObject);
        }
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void addToScore(int value)
    {
        score += value;
        scoreSlider.value = getScorePercentage();
        Image fillImage = scoreSlider.transform.GetChild(1).GetChild(0).GetComponent<Image>();
        if(getScorePercentage() <= 33)
            fillImage.color = new Color(255, 0,0);
        else if(getScorePercentage() <= 66)
            fillImage.color = new Color(255, 210, 0);
        else
            fillImage.color = new Color(0, 255, 0);
    }

    public void resetScore()
    {
        score = 0;
    }

    public void setScoreSlider(Slider slider)
    {
        scoreSlider = slider;
    }

    public void increaseMaxScore(int value)
    {
        maximumScore += value;
    }

    public float getScorePercentage()
    {
        return (score / (float)maximumScore) * 100;
    }
}
