﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GuitareController : MonoBehaviour {

    public static bool running = false;
    public Transform FailY;

    public GameObject[] keys;
    private KeyCode[] keyCodes = new KeyCode[4] { KeyCode.A, KeyCode.Z, KeyCode.E, KeyCode.R };
    public Sprite[] pressedSprites, unpressedSprites;

    public GameObject successParticleSystem;

    //Counter
    public GameObject guitareCounter;
    public int maxSerie;
    private GuitareCounterController guitareCounterController;
    private int serie;

    public float keySizeX, keySizeY;

    public float notePerSecond;
    private float nextTimeNoteCreation = 0;

    public Slider scoreSlider;

    //NOTES
    public GameObject notePrefab;
    public Transform[] noteSpawns;
    public Sprite[] noteSprites;
    public int noteValue, nbTotalNote;
    private int noteCreated = 0;
    private bool nextSceneCalled = false;

    //Sounds
    public AudioClip failSound;
    public AudioClip[] musics;

    private AudioSource audio;
    private float musicStartTime = 0;
    private bool playing = false;

	// Use this for initialization
	void Start () {
        NoteController.guitare = this;
        audio = GetComponent<AudioSource>();
        GameController.controller.setScoreSlider(scoreSlider);
        GameController.controller.addToScore(0);

        initGuitareCounter();
    }

    private void initGuitareCounter()
    {
        serie = maxSerie / 2;
        guitareCounterController = guitareCounter.GetComponent<GuitareCounterController>();
        guitareCounterController.setMaxSerie(maxSerie);
        guitareCounterController.updateIndicator(serie);
    }

    // Update is called once per frame
    void Update()
    {
        if(running)
        {
            checkKeys();
            createNewNote();
        }
    }

    private void createNewNote()
    {
        if(Time.time >= nextTimeNoteCreation)
        {
            nextTimeNoteCreation = Time.time + 1 / notePerSecond;
            int value = UnityEngine.Random.Range(0, 5);
            if(value > 2 && noteCreated < nbTotalNote)
            {
                int spawnIndex = UnityEngine.Random.Range(0, 4);
                GameObject note =  Instantiate(notePrefab, noteSpawns[spawnIndex].position, Quaternion.identity);
                note.GetComponent<SpriteRenderer>().sprite = noteSprites[spawnIndex];
                noteCreated++;
            } else if(noteCreated >= nbTotalNote)
            {
                if(GameController.controller.interviewData.interviewFinished())
                {
                    Invoke("loadEnd", 5);
                } else
                {
                    Invoke("loadInterview", 5);
                }
            }
        }
    }

    private void loadInterview()
    {
        SceneManager.LoadScene(3);
    }

    private void loadEnd()
    {
        SceneManager.LoadScene(5);
    }


    public void onNoteFail()
    {
        audio.time = 0;
        audio.clip = failSound;
        audio.Play();
        GameController.controller.increaseMaxScore(noteValue);
    }

    private void checkKeys()
    {
        for(int i = 0; i < 4; i++)
        {
            if (Input.GetKeyDown(keyCodes[i]))
                onKeyDown(i);
            else if (Input.GetKeyUp(keyCodes[i]))
                onKeyUp(i);
        }
    }

    private void onKeyUp(int index)
    {
        keys[index].GetComponent<SpriteRenderer>().sprite = unpressedSprites[index];
    }

    private void onKeyDown(int index)
    {
        keys[index].GetComponent<SpriteRenderer>().sprite = pressedSprites[index];
        string[] arr = { "Note" };
        Collider2D note = Physics2D.OverlapBox(
            keys[index].transform.position,
            new Vector2(keySizeX, keySizeY),
            0,
            LayerMask.GetMask(arr));
        if (note)
        {
            Instantiate(successParticleSystem, note.transform.position, Quaternion.identity);
            Destroy(note.gameObject);
            if(musicStartTime == 0)
            {
                audio.clip = musics[0];
                audio.Play();
                musicStartTime = Time.time;
                playing = true;
            } else if(!playing)
            {
                audio.clip = musics[0];
                audio.Play();
                audio.time = Time.time - musicStartTime;
                playing = true;
            }
            GameController.controller.addToScore(noteValue);

            updateSerie(1);
        } else
        {
            if (musicStartTime == 0)
                musicStartTime = Time.time;
            
            audio.clip = failSound;
            audio.time = 0;
            audio.Play();
            playing = false;
            GameController.controller.addToScore(-1);
        }

        GameController.controller.increaseMaxScore(noteValue);
    }

    private void updateSerie(int addToSerie)
    {
        if (serie > maxSerie)
            serie = maxSerie;
        else if (serie < 0)
            serie = 0;
        else
            serie += addToSerie;

        guitareCounterController.updateIndicator(serie);
    }
}
