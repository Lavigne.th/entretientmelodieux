﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/*
 * Pour le compte a rebours
 */
public class CounterController : MonoBehaviour {

    public GameObject panel;
    public Sprite[] numbers;

	// Use this for initialization
	void Start () {
        Invoke("switch2", 1.3f);
        Invoke("switch1", 2.3f);
        Invoke("switch0", 3.3f);
        Invoke("delete", 4.5f);
    }
	
	void switch2()
    {
        GetComponent<Image>().sprite = numbers[2];
    }

    void switch1()
    {
        GetComponent<Image>().sprite = numbers[1];
    }

    void switch0()
    {
        GetComponent<Image>().sprite = numbers[0];
    }

    void delete () {
        GuitareController.running = true;
        Destroy(panel);
	}
}
