﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NoteController : MonoBehaviour {

    public float speed;
    public static GuitareController guitare;
    public float maxY;

    private bool missed = false;

    // Use this for initialization
    void Start () {
        
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void FixedUpdate()
    {
        transform.position = new Vector3(
            transform.position.x,
            transform.position.y + speed,
            transform.position.z
            );
        if(transform.position.y < maxY && !missed)
        {
            missed = true;
            guitare.onNoteFail();
            Invoke("kill", 2);
        }
    }

    private void kill()
    {
        Destroy(gameObject);
    }
}
