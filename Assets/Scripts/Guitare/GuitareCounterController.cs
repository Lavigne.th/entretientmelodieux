﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Anima2D;
using System;

public class GuitareCounterController : MonoBehaviour {

    public Bone2D indicator;
    //0 => red, 1 => yellow, 2 => green
    public Sprite[] sprites;
    public float angleWideness, minAngle, maxAngle;
    private float maxSerie, currentAngle;
    private Stage stage = Stage.YELLOW;

    private enum Stage
    {
        RED, YELLOW, GREEN
    }

	// Use this for initialization
	void Start () {
        maxAngle = minAngle + angleWideness;
    }
	
	// Update is called once per frame
	void Update () {
        
    }

    public void updateIndicator(int serie)
    {
        float ratio = serie / (float)maxSerie;
        currentAngle = minAngle + ratio * angleWideness;
        if (currentAngle < minAngle)
            currentAngle = minAngle;
        else if (currentAngle > maxAngle)
            currentAngle = maxAngle;
        indicator.transform.localEulerAngles = new Vector3(0, 0, currentAngle);

        manageSprite(ratio);
    }

    private void manageSprite(float ratio)
    {
        if (stage != Stage.GREEN && ratio > 0.666f)
        {
            GetComponent<SpriteRenderer>().sprite = sprites[2];
            stage = Stage.GREEN;
        } 
        else if(stage != Stage.YELLOW && ratio > 0.333f && ratio < 0.666f)
        {
            GetComponent<SpriteRenderer>().sprite = sprites[1];
            stage = Stage.YELLOW;
        }
        else if (stage != Stage.RED && ratio < 0.333f)
        {
            GetComponent<SpriteRenderer>().sprite = sprites[0];
            stage = Stage.RED;
        }
         
    }

    public void setMaxSerie(int maxSerie)
    {
        this.maxSerie = maxSerie;
    }
}
