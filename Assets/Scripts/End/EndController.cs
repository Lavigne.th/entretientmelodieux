﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndController : MonoBehaviour {

    public GameObject[] stars;
    public GameObject particle;

    public GameObject firedTampon, HiredTampon;

    public GameObject firedBackground, hiredBackground;

	// Use this for initialization
	void Start () {
        Instantiate(stars[0]);

        int nbStar = getNbStars();
        float delayTampon = 0;
        for(int i = 0; i< nbStar; i++)
        {
            string funcName = String.Format("star{0}", i);
            Invoke(funcName, 0.5f * i);
            delayTampon += 0.5f;
        }

        if(nbStar < 4)
        {
            Instantiate(firedBackground);
            Invoke("createFiredTampon", 2f + delayTampon);
        } else
        {
            Instantiate(hiredBackground);
            Invoke("createHiredTampon", 2f + delayTampon);
        }

        

        //Invoke("star1", .5f);
        //Invoke("star2", 1f);
        //Invoke("star3", 1.5f);
        //Invoke("star4", 2f);
    }

    private int getNbStars()
    {
        float ratio = GameController.controller.getScorePercentage();
        if (ratio < 20)
            return 1;
        if (ratio < 40)
            return 2;
        if (ratio < 60)
            return 3;
        if (ratio < 80)
            return 4;
        return 5;
    }

    void star0()
    {
        Instantiate(stars[0], stars[0].transform.position, Quaternion.identity);
    }

    void star1()
    {
        Instantiate(stars[1], stars[1].transform.position, Quaternion.identity);
    }

    void star2()
    {
        Instantiate(stars[2], stars[2].transform.position, Quaternion.identity);
    }

    void star3()
    {
        Instantiate(stars[3], stars[3].transform.position, Quaternion.identity);
    }

    void star4()
    {
        Instantiate(stars[4], stars[4].transform.position, Quaternion.identity);
    }

    void createFiredTampon()
    {
        Instantiate(firedTampon);
    }

    void createHiredTampon()
    {
        Instantiate(HiredTampon);
    }
    

    // Update is called once per frame
    void Update () {
		
	}
}
