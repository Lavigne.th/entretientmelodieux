﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Assets.Scripts.Interview;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System;

public class InterviewController : MonoBehaviour {

    public Text questionText;
    public Button[] answerButtons;

    public Sprite defaultAnswer, rightAnswer, wrongAnswer;

    public Slider scoreSlider;

    public InterviewData interviewData;

    public int questionPositiveScore;

    // Use this for initialization
    void Start () {
        interviewData = GameController.controller.interviewData;
        displayQuestion();
        GameController.controller.setScoreSlider(scoreSlider);
        GameController.controller.addToScore(0);
    }



    private void displayQuestion()
    {
        
        if(interviewData.interviewFinished())
        {
            SceneManager.LoadScene(2);
        } else
        {
            Question question = (Question)interviewData.getCurrentQuestion();
            questionText.text = question.question;
            for(int i = 0; i < 4; i++)
            {
                answerButtons[i].transform.GetChild(0).GetComponent<Text>().text = question.answers[i];
                answerButtons[i].GetComponent<Image>().sprite = defaultAnswer;
            }
                
        }
    }

    public void onAnswerChosen(int index)
    {
        Question question = (Question)interviewData.getCurrentQuestion();
        question.answerChosen = index;
        answerButtons[question.anwserIndex].GetComponent<Image>().sprite = rightAnswer;
        if (!question.answeredWell())
        {
            answerButtons[index].GetComponent<Image>().sprite = wrongAnswer;
        } else
        {
            GameController.controller.addToScore(questionPositiveScore);
        }
        GameController.controller.increaseMaxScore(questionPositiveScore);
        Invoke("switchToReactionScene", 3);
    }

    private void switchToReactionScene()
    {
        SceneManager.LoadScene(3);
    }

    // Update is called once per frame
    void Update () {
		
	}
}
