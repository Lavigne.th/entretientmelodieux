﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.Interview
{
    public class InterviewData
    {
        public int currentQuestionIndex = 0;

        private ArrayList questions = new ArrayList();

        public InterviewData()
        {
            loadQuestions();
        }

        private void loadQuestions()
        {
            questions.Add(
                new Question(
                    "Quelle est la capitale des îles Vanuatu",
                    new string[] { "A : Tachkent", "B : Astana", "C : Port Villa", "D : Paris" },
                    2,
                    new string[] { "Nop!", "Raté!", "Je suis d'accord!", "Quoi? Vous rigolez j'espere?" }
                    ));

            questions.Add(
               new Question(
                   "Au fait, on chercher quelqu'un pour jouer de la guitare!",
                   new string[] { "A : Je gere LOL", "B : Je veux bien essayer", "C : Je ne suis pas intérréssé", "D : Je sais jouer du pipo aussi?" },
                   2,
                   new string[] { "LOL! LOL! On va bien voir! MDR", "Essayer? C'est tout?", "Vous avez raison c'est chiant mais faut le faire", "Vous vous foutez de moi? Allez! A la guitare!" },
                   true
                   ));
        }

        public Question getCurrentQuestion()
        {
            return (Question)questions[currentQuestionIndex];
        }

        public bool interviewFinished()
        {
            return questions.Count == currentQuestionIndex;
        }

        public void nextQuestion()
        {
            currentQuestionIndex++;
        }

        
    }
}
