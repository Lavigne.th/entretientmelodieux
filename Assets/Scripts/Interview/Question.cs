﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.Interview
{
    public class Question
    {
        public string question;
        public string[] answers;
        public int anwserIndex, answerChosen;
        public string[] reactions;

        public bool followedByGuitar = false;

        public Question(string question, string[] answers, int anwserIndex, string[] reactions)
        {
            this.question = question;
            this.answers = answers;
            this.anwserIndex = anwserIndex;
            this.reactions = reactions;
        }

        public Question(string question, string[] answers, int anwserIndex, string[] reactions, bool followedByGuitar)
        {
            this.question = question;
            this.answers = answers;
            this.anwserIndex = anwserIndex;
            this.reactions = reactions;
            this.followedByGuitar = followedByGuitar;
        }

        public string getReaction()
        {
            return reactions[answerChosen];
        }

        public bool answeredWell()
        {
            return answerChosen == anwserIndex;
        }
    }
}
