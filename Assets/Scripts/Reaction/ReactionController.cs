﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Assets.Scripts.Interview;
using UnityEngine.SceneManagement;

public class ReactionController : MonoBehaviour {


    public Text bossReactionText;
    public GameObject angryBoss, happyBoss, neutralBoss;

    private InterviewData interviewData;

	// Use this for initialization
	void Start () {
        interviewData = GameController.controller.interviewData;
        manageReaction();
    }

    private void manageReaction()
    {
        Question question = interviewData.getCurrentQuestion();
        if(question.answeredWell())
        {
            //Instantiate(happyBoss);
        } else
        {
            Instantiate(angryBoss);
        }
        bossReactionText.text = question.getReaction();
        if (question.followedByGuitar)
            Invoke("guitarScene", 5);
        else
            Invoke("interviewScene", 5);
        interviewData.nextQuestion();
    }

    private void guitarScene()
    {
        SceneManager.LoadScene(4);
    }
    public void interviewScene()
    {
        SceneManager.LoadScene(2);
    }

    // Update is called once per frame
    void Update () {
		
	}
}
